function iShoot(enemy){

    enemy.classList.add("dead");

}

function enemyAttacksMe(enemy){
    
    enemy.classList.add("showing");

    setTimeout(()=>{

        enemyShootsMe(enemy);
    },1000);

    setTimeout(()=>{

        enemy.classList.remove("showing");
    },3000);
}

var enemy1=document.querySelector("#enemy1");
enemyAttacksMe(enemy1);

function enemyShootsMe(enemy){

    enemy.classList.add("shooting");

    setTimeout(()=>{

        enemy.classList.remove("shooting");
    },200);
}